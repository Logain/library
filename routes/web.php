<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/add', function () {
    return view('addBook');
})->name('addBook');

Route::post('/book/submit', 'BookController@submit')->name('form');

Route::get('/books/all', 'BookController@allData')->name('books-data');

Route::get('/books/all/{id}', 'BookController@showOneBook'
    )->name('book-data-one');

Route::get('/books/all/{id}/update', 'BookController@updateBook'
)->name('book-update');

Route::post('/books/all/{id}/update', 'BookController@updateBookSubmit'
)->name('book-update-submit');

Route::get('/books/all/{id}/delete', 'BookController@deleteBook'
)->name('book-delete');

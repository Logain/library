<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookRequest;
use App\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function submit(BookRequest $req) {
        $book = new Book();
        $book->name = $req->input('name');
        $book->email = $req->input('email');
        $book->subject = $req->input('subject');
        $book->message = $req->input('message');
        $book->save();

        return redirect()->route('home')->with('success', 'Congratulations, your book added!');

        /*$validation = $req -> validate([
            'name' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'message' => 'required'
        ]);*/
    }

    public function allData() {
        $book = new Book();
        return view('books', ['data' => $book->all()]);
        //dd($contact -> all());
    }

    public function showOneBook($id) {
        $book = new Book();
        return view('one-book', ['data' => $book->find($id)]);
    }

    public function updateBook($id) {
        $book = new Book();
        return view('update-book', ['data' => $book->find($id)]);

    }

    public function updateBookSubmit($id, BookRequest $req) {
        $book = Book::find($id);
        $book->name = $req->input('name');
        $book->email = $req->input('email');
        $book->subject = $req->input('subject');
        $book->message = $req->input('message');
        $book->save();

        return redirect()->route('book-data-one', $id)->with('success', 'Your book successfully updated!');
    }

    public function deleteBook($id) {
        Book::find($id)->delete();
        return redirect()->route('books-data')->with('success', 'Your book successfully removed!');
    }
}

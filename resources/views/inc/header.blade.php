@section('header')
<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
    <h3 class="my-0 mr-md-auto font-weight-normal">Library</h3>
    <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="{{route('home')}}">Home</a>
        <a class="p-2 text-dark" href="{{route('books-data')}}">Edit</a>
        <a class="p-2 text-dark" href="{{route('addBook')}}">Add book</a>
        <a class="p-2 text-dark" href="{{route('about')}}">Borrowed</a>
    </nav>
    <a class="btn btn-outline-primary" href="#">Sign up</a>
</div>

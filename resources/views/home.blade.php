@extends('layouts.app')

@section('title-block')
Home
@endsection

@section('content')
<h1>Home</h1>
    <p>Just a text</p>
    @section('aside')
        @parent
        <p>Additional text</p>
    @endsection
@endsection

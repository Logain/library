@extends('layouts.app')

@section('title-block')Edit message
@endsection

@section('content')

    <form action="{{route('book-update-submit', $data->id)}}" method="post">
        @csrf
        <div class="form-group">
            <label for="name">Enter the name</label>
            <input type="text" name="name" value="{{$data->name}}" placeholder="Enter the name" id="name" class="form-control">
        </div>
        <div class="form-group">
            <label for="email">Enter the email</label>
            <input type="text" name="email" value="{{$data->email}}" placeholder="Enter the email" id="email" class="form-control">
        </div>
        <div class="form-group">
            <label for="subject">Enter the subject</label>
            <input type="text" name="subject" value="{{$data->subject}}" placeholder="Enter the subject" id="subject" class="form-control">
        </div>
        <div class="form-group">
            <label for="message">Message</label>
            <textarea name="message" id="subject" class="form-control">{{$data->message}}</textarea>
        </div>
        <button type="submit" class="btn btn-success">Update</button>
    </form>

@endsection

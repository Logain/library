@extends('layouts.app')

@section('title-block')
Books
@endsection

@section('content')

    <form action="{{route('form')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="name">Enter the name</label>
            <input type="text" name="name" placeholder="Enter the name" id="name" class="form-control">
        </div>
        <div class="form-group">
            <label for="email">Enter the email</label>
            <input type="text" name="email" placeholder="Enter the email" id="email" class="form-control">
        </div>
        <div class="form-group">
            <label for="subject">Enter the subject</label>
            <input type="text" name="subject" placeholder="Enter the subject" id="subject" class="form-control">
        </div>
        <div class="form-group">
            <label for="message">Message</label>
            <textarea name="message" id="subject" class="form-control"></textarea>
        </div>
        <button type="submit" class="btn btn-success">Send</button>
    </form>

@endsection
